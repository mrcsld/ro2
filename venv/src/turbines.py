"""
 * @file turbines.py
 * @brief Definition of Turbine and Cable class.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Soldan (<marco.soldan@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2017-2018 University of Padua, Italy
 * @copyright Apache License, Version 2.0
"""
import math


# Definition of Turbine class #
class Turbine:

    # Variables definition
    xCoord = -1
    yCoord = -1
    type = 0

    # Constructor
    def __init__(self, X, Y, T):
        self.xCoord = int(X)
        self.yCoord = int(Y)
        self.type = int(T)

    # To string function
    def __str__(self):
        return "X: " + str(self.xCoord) + " Y: " + str(self.yCoord) + " T: " + str(self.type)

    def dist(self, T):
        """ Calculates the distance between two turbines

            :param T : other turbine to calculate the distance from
            :return : euclidean distance from the other Turbine
            """
        d = math.sqrt(math.pow(self.xCoord - T.xCoord, 2) + math.pow(self.yCoord - T.yCoord, 2))
        return d

    # Return the type of a turbine. +1 if is a normal turbine, -1 if it is a substation
    def get_T(self):
        return self.type

    # Return the X coordinate of a Turbine
    def get_X(self):
        return self.xCoord

    # Return the Y coordinate of a Turbine
    def get_Y(self):
        return self.yCoord


# Definition of Cable class #
class Cable:

    # Variables definition
    capacity = -1
    price = -1
    max_usage = -1

    # Constructor
    def __init__(self, C, P, M):
        self.capacity = int(C)
        self.price = float(P)
        self.max_usage = int(M)

    # To string method
    def __str__(self):
        return "Capacity: " + str(self.capacity) + " Price: " + str(self.price) + " Max Usage: " + str(self.max_usage)

    # Return the capacity of a cable
    def get_capacity(self):
        return self.capacity

    # Return the price of a cable
    def get_price(self):
        return self.price
