"""
 * @file main.py
 * @brief This is the main function.
 * Based on the choice of the user it will build the model and launch the resolution method
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Soldan (<marco.soldan@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2017-2018 University of Padua, Italy
 * @copyright Apache License, Version 2.0
"""


import model
import heuristic
import loopMethod
import time

def main():

    # Read the data and build the model
    cM = model.CPLXModel("../data/", "../model/")
    cM.read_data(cM.turbFile, cM.cableFile, dummyCable=cM.dummy)
    results_path = "../results/"

    if cM.choice == 1:  # Solve with lazy constraint
        cM.build_model(lazy=1)
        start_time = cM.cplexModel.get_time()
        cM.solve_model()
        end_time = cM.cplexModel.get_time()
        print(cM.cplexModel.solution.MIP.get_best_objective())
        print(cM.cplexModel.solution.MIP.get_mip_relative_gap())
        print(cM.cplexModel.solution.get_objective_value())
        cM.bestSol = cM.cplexModel.solution.get_values()
        cM.bestZ = cM.cplexModel.solution.get_objective_value()
        f = open(results_path + "LAZY_CONSTRAINTS.txt", "a")
        f.write("dataset: " + str(cM.turbFile) + ", time_limit: " + str(cM.timeLimit) +
                ", choice: " + str(cM.choice) + ", best cost: " + str(cM.cplexModel.solution.get_objective_value()) +
                ", gap:" + str(cM.cplexModel.solution.MIP.get_mip_relative_gap()) +
                ", rins: " + str(cM.cplexModel.parameters.mip.strategy.rinsheur.get()) +
                ", polish_after: " + str(cM.cplexModel.parameters.mip.polishafter.time.get()) +
                ", time_spent: " + str(end_time-start_time) + "\n")
    elif cM.choice == 2:  # Solve with loop method
        cM.build_model()
        start_time = cM.cplexModel.get_time()
        loopMethod.solveWithLoop(cM)
        end_time = cM.cplexModel.get_time()
        f = open(results_path + "LOOP_METHOD.txt", "a")
        f.write("dataset: " + str(cM.turbFile) + ", time_limit: " + str(cM.timeLimit) +
                ", choice: " + str(cM.choice) + ", best cost: " + str(cM.bestZ) +
                ", gap:" + str(cM.cplexModel.solution.MIP.get_mip_relative_gap()) +
                ", rins: " + str(cM.cplexModel.parameters.mip.strategy.rinsheur.get()) +
                ", time_spent: " + str(end_time - start_time) + "\n")
        cM.draw_solution(cM.bestSol, multiFigure=False)

    elif cM.choice == 3:  # Solve with callback
        callback = cM.cplexModel.register_callback(model.NoCrossCallback)
        callback.model = cM
        cM.build_model()
        start_time = cM.cplexModel.get_time()
        loopMethod.solveWithLoop(cM)
        end_time = cM.cplexModel.get_time()


        f = open(results_path + "CALLBACK.txt", "a")
        f.write("dataset: " + str(cM.turbFile) + ", time_limit: " + str(cM.timeLimit) +
                ", choice: " + str(cM.choice) + ", best cost: " + str(cM.bestZ) +
                ", gap:" + str(cM.cplexModel.solution.MIP.get_mip_relative_gap()) +
                ", rins: " + str(cM.cplexModel.parameters.mip.strategy.rinsheur.get()) +
                ", time_spent: " + str(end_time - start_time) + "\n")

    elif cM.choice == 4:  # Solve with hard fixing heuristic
        callback = cM.cplexModel.register_callback(model.NoCrossCallback)
        callback.model = cM
        cM.build_model()
        start_time = cM.cplexModel.get_time()
        heuristic.hardFixing(cM, iterationTime=20)
        print(cM.bestZ)
        cM.draw_solution(cM.bestSol, multiFigure=False, solution_cost=cM.bestZ)
        end_time = cM.cplexModel.get_time()
        f = open(results_path + "HARDFIXING.txt", "a")
        f.write("dataset: " + str(cM.turbFile) + ", time_limit: " + str(cM.timeLimit) +
                ", choice: " + str(cM.choice) + ", best cost: " + str(cM.bestZ) +
                ", gap:" + str(cM.cplexModel.solution.MIP.get_mip_relative_gap()) +
                ", rins: " + str(cM.cplexModel.parameters.mip.strategy.rinsheur.get()) +
                ", time_spent: " + str(end_time - start_time) + "\n")

    elif cM.choice == 5:  # Solve with soft fixing heuristic
        callback = cM.cplexModel.register_callback(model.NoCrossCallback)
        callback.model = cM
        cM.build_model()
        start_time = cM.cplexModel.get_time()
        heuristic.softFixing(cM, iterationTime=600)
        end_time = cM.cplexModel.get_time()
        f = open(results_path + "SOFTFIXING.txt", "a")
        f.write("dataset: " + str(cM.turbFile) + ", time_limit: " + str(cM.timeLimit) +
                ", choice: " + str(cM.choice) + ", best cost: " + str(cM.bestZ) +
                ", gap:" + str(cM.cplexModel.solution.MIP.get_mip_relative_gap()) +
                ", rins: " + str(cM.cplexModel.parameters.mip.strategy.rinsheur.get()) +
                ", time_spent: " + str(end_time - start_time) + "\n")

    elif cM.choice == 6:  # Solve with greedy grasp
        heuristic.greedy(cM, 100)

    elif cM.choice == 7:  # Solve with taboo search
        [sol, cost, avg_time] = heuristic.taboo_search(cM, cM.substationCapacity, cM.initialization_method, cM.tenure, cM.timeout)
        f = open(results_path + "TABOO.txt", "a")
        f.write("dataset: " + str(cM.turbFile) + ", timeout: " + str(cM.timeout) + ", initialization method: " + str(cM.initialization_method) + ", tenure: " + str(cM.tenure) + ", avg_iteration_time: " + str(avg_time) + ", cost: " + str(cost))

    cM.draw_solution(sol=cM.bestSol, save_fig=True)

    
    print("Press CTRL-C two times to exit, the program will automatically exit in 15 seconds")
    exit_time = time.time() + 15
    while time.time() < exit_time:
        1

if __name__ == '__main__':
    main()
