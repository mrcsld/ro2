"""
 * @file turbines.py
 * @brief Solves the model with loop method.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Soldan (<marco.soldan@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2017-2018 University of Padua, Italy
 * @copyright Apache License, Version 2.0
"""
import time


def solveWithLoop(cM, solve1Time=False):

    # Variables definition
    cnt = 0  # Loop number
    timeout = time.time() + cM.timeout
    while (cM.cplexModel.solution.get_status() is not cM.cplexModel.solution.status.optimal
           and cM.cplexModel.solution.get_status() is not cM.cplexModel.solution.status.optimal_tolerance) \
            and time.time() < timeout:

        # Get the current solution
        [solution, z] = cM.solve_model()

        cnt += 1

        # Checks whether cplex could find a feasible solution, if not it skips
        if solution is None or z is None:
            continue


        # Checks the presence of crossings in the model, if they are found cuts are added
        cuts = cM.nocross_separation(solution)
        for cut in cuts:
            cM.nocross_cut(cut[0], cut[1], cut[2])

        # If the solution didn't present crossings its saved
        if len(cuts) == 0:
            cM.update_best(solution, z)

        cM.draw_solution(cM.bestSol, iteration=cnt, multiFigure=False)

        if solve1Time is True:
            break

    #time.sleep(10)  # To see the plot
