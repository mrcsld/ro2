"""
 * @file geom_utils.py
 * @brief Implementation of class Point and intersection check methods.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Soldan (<marco.soldan@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2017-2018 University of Padua, Italy
 * @copyright Apache License, Version 2.0
"""


# Definition of Point class #
class Point:

    # Variables definition
    x = None
    y = None

    # Constructor
    def __init__(self, x, y):
        """

        :param x: x value
        :param y: y value
        """
        self.x = x
        self.y = y


def intersects(p1, p2, p3, p4):
    """ Check if there is an intersection between the segment formed by p1p2 and p3p4

    :param p1: First point of line 1
    :param p2: Second point of line 1
    :param p3: First point of Line 2
    :param p4: Second point of Line 2
    :return: +1 if an intersection occurs, 0 otherwise
    """

    # Determinant computation
    det = ((p4.x-p3.x)*(-(p2.y-p1.y))) - ((-(p2.x-p1.x))*(p4.y-p3.y))

    if det == 0:  # No intersection
        return 0

    detmu = ((p1.x-p3.x)*(-(p2.y-p1.y))) - ((-(p2.x-p1.x))*(p1.y-p3.y))
    detlambda = ((p4.x-p3.x)*(p1.y-p3.y)) - ((p1.x-p3.x)*(p4.y-p3.y))

    mu = detmu/det
    vlambda = detlambda/det

    if 0 < mu < 1 and 0 < vlambda < 1:
        return 1  # Intersection found
    else:
        return 0  # No intersection found


def intersection_point(p1, p2, p3, p4):
    """ Given 2 intersecting lines returns the intersection point

    :param p1: First point of line 1
    :param p2: Second point of line 1
    :param p3: First point of Line 2
    :param p4: Second point of Line 2
    :return: The intersection Point
    """
    # Determinant computation
    det = ((p4.x - p3.x) * (-(p2.y - p1.y))) - ((-(p2.x - p1.x)) * (p4.y - p3.y))

    if det == 0:  # No intersection
        return 0

    detlambda = ((p4.x - p3.x) * (p1.y - p3.y)) - ((p1.x - p3.x) * (p4.y - p3.y))

    dx = p2.x - p1.x
    dy = p2.y - p1.y

    intersection = [p1.x + detlambda/det * dx, p1.y + detlambda/det * dy]

    return Point(intersection[0], intersection[1])
