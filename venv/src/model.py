"""
 * @file turbines.py
 * @brief This class contain the model and some utility functions.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Soldan (<marco.soldan@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2017-2018 University of Padua, Italy
 * @copyright Apache License, Version 2.0
"""
from random import *

import cplex
import sys
import time
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import math
import numpy as np
import pandas as pd
import seaborn as sns
import turbines
import geom_utils


# Definition of model class #
class CPLXModel:

    # Constructor
    def __init__(self, data_folder, model_folder):
        """Initialize the model to an empty state, also adds the data folder

        :param data_folder:folder in which are contained the data files
        """

        # Auxiliary parameters
        self.inf_cplex = cplex.infinity

        # Heuristic parameters
        self.tenure = 100
        self.initialization_method = 2

        # solution parameters
        self.bestSol = []
        self.bestZ = self.inf_cplex

        # main parameters
        self.choice = 1
        self.dummy = False

        # I/O parameters
        self.data_folder = data_folder
        self.model_folder = model_folder
        self.modelTurb = []
        self.modelCables = []
        self.cplexModel = cplex.Cplex()

        # Sets parameters given from command line
        self.turbFile = None
        self.cableFile = None
        self.timeLimit = self.cplexModel.parameters.timelimit.get()
        self.timeout = math.inf
        self.substationCapacity = 7  # Default value, shouldn't create a real constraint

        self.rins = self.cplexModel.parameters.mip.strategy.rinsheur.get()
        self.polish = self.cplexModel.parameters.mip.polishafter.time.get()
        self.read_command_line()
        self.cplexModel.parameters.timelimit.set(self.timeLimit)
        self.cplexModel.parameters.mip.strategy.rinsheur.set(self.rins)
        self.cplexModel.parameters.mip.polishafter.time.set(self.polish)

        # Number of stations/cables
        self.nNodes = -1
        self.nCables = -1

        # Starting index of the y,f,x variables
        self.yOffset = 0
        self.fOffset = 0
        self.xOffset = 0
        self.sOffset = 0

    def read_data(self, turbName, cableName, dummyCable=False):
        """ Reads all the data for the model

        :param turbName: turbines file name
        :param cableName: cables file name
        :param dummyCable: enable cable with "infinite capacity"
        :return: Nothing
        """
        self.read_turbP(turbName)
        self.read_cableP(cableName, dummyCable)

        return

    def read_turbP(self, filename):
        """ Read the Turibine file

        :param filename: Filename where Turbine data is stored
        :return: Nothing
        """
        path = self.data_folder + filename
        # Parse file with Pandas
        file = pd.read_table(path, header=None, sep="\t", engine='python', delim_whitespace=True)

        for i in range(0, len(file.index)):
            self.modelTurb.append(turbines.Turbine(file[0][i], file[1][i], file[2][i]))  # Store (x,y,t)

        self.nNodes = len(self.modelTurb)
        return

    def read_cableP(self, filename, dummyCable):
        """ ead the Cable file

        :param filename: Filename where Cable data is stored40923670.22
        :param dummyCable: True if use cable with infinite capacity, 0 otherwise
        :return: Nothing
        """

        path = self.data_folder + filename
        # Parse file with Pandas
        file = pd.read_table(path, header=None, sep="\t", engine='python', delim_whitespace=True)

        for i in range(0, len(file.index)):
            self.modelCables.append(turbines.Cable(file[0][i], file[1][i], file[2][i]))  # Store (c,p,m)

        if dummyCable is True:
            self.modelCables.append(turbines.Cable(1e10, 1e10, 1e10))

        self.nCables = len(self.modelCables)
        return

    def build_model(self, lazy=0, randomSeed=1):
        """ Adds the model constraints

        :param lazy: 1 if the model should use lazy constraints, 0 otherwise
        :return: 0 if all right, negative number otherwise
        """

        if randomSeed == 1:
            self.cplexModel.parameters.randomseed.set(randint(0, pow(2,16)))

        # Y variables #
        self.yOffset = self.cplexModel.variables.get_num()

        # Bound on Yij variables
        low_bnd = [0.0]
        up_bnd = [1.0]

        # Add Y variables
        for i in range(0, self.nNodes):
            for j in range(0, self.nNodes):
                if i != j:
                    self.cplexModel.variables.add(names=["Y(" + str(i + 1) + "," + str(j + 1) + ")"], lb=low_bnd,
                                                  ub=up_bnd, types=self.cplexModel.variables.type.binary)
                if i == j:
                    self.cplexModel.variables.add(names=["Y(" + str(i + 1) + "," + str(j + 1) + ")"], lb=low_bnd,
                                                  ub=low_bnd, types=self.cplexModel.variables.type.binary)

        # Out-degree constraints
        for i in range(0, self.nNodes):
            indexes = self.out_indexes_y(i)
            values = [1] * len(indexes)
            lhs = cplex.SparsePair(indexes, values)
            typo = self.modelTurb[i].get_T()

            # If it is a turbine then sum = 1, otherwise it's substation then sum = 0
            if typo >= -0.5:
                self.cplexModel.linear_constraints.add(lin_expr=[lhs], senses=['E'], rhs=[1],
                                                       names=["Out Degree of Y" + str(i + 1) + "j"])
            else:
                self.cplexModel.linear_constraints.add(lin_expr=[lhs], senses=['E'], rhs=[0],
                                                       names=["Out Degree of Y" + str(i + 1) + "j"])

        # In-degree constraints
        substationCapacityConstraints = []
        for i in range(0, self.nNodes):
            typo = self.modelTurb[i].get_T()

            # Substation case
            if typo <= -0.5:
                indexes = self.in_indexes_y(i)
                values = [1] * len(indexes)
                lhs = cplex.SparsePair(indexes, values)
                rhs = self.substationCapacity
                substationCapacityConstraints += list(self.cplexModel.linear_constraints.add(
                    lin_expr=[lhs], senses=['L'], rhs=[rhs], names=["In Degree of Yi" + str(i + 1)]))

        # Flow Variables #
        self.fOffset = self.cplexModel.variables.get_num()

        # Add f variables
        for i in range(0, self.nNodes):
            for j in range(0, self.nNodes):
                if i != j:
                    self.cplexModel.variables.add(names=["F(" + str(i + 1) + "," + str(j + 1) + ")"], lb=low_bnd,
                                                  types=self.cplexModel.variables.type.continuous)
                if i == j:
                    self.cplexModel.variables.add(names=["F(" + str(i + 1) + "," + str(j + 1) + ")"], lb=low_bnd,
                                                  ub=low_bnd, types=self.cplexModel.variables.type.continuous)

        # Flow constraints
        flowConstraints = []
        for i in range(0, self.nNodes):
            typo = self.modelTurb[i].get_T()
            production = 1  # default value

            # Turbine case
            if typo >= -0.5:
                outIndexes = self.out_indexes_f(i)
                inIndexes = self.in_indexes_f(i)
                indexes = outIndexes + inIndexes
                values = [1] * len(outIndexes) + [-1] * len(inIndexes)
                # Duplicate in the flow cycle, we delete one of the occurences
                indRemove = indexes.index(self.fij_index(i, i))
                indexes.pop(indRemove)
                values.pop(indRemove)
                lhs = cplex.SparsePair(indexes, values)
                rhs = production
                flowConstraints += list(self.cplexModel.linear_constraints.add(
                    names=["Sum of Fhj equals Sum of Fih plus production"], lin_expr=[lhs], senses=['E'], rhs=[rhs]))

        # Cable Variables #
        self.xOffset = self.cplexModel.variables.get_num()

        # Bound on xij variables
        low_bnd = 0.0
        up_bnd = 1.0

        # Add cable variables
        for i in range(0, self.nNodes):
            for j in range(0, self.nNodes):
                for k in range(0, self.nCables):
                    if i != j:
                        self.cplexModel.variables.add(
                            names=["X(" + str(i + 1) + "," + str(j + 1) + "," + str(k + 1) + ")"], lb=[low_bnd],
                            ub=[up_bnd], types=self.cplexModel.variables.type.binary)
                    else:
                        self.cplexModel.variables.add(
                            names=["X(" + str(i + 1) + "," + str(j + 1) + "," + str(k + 1) + ")"], lb=[low_bnd],
                            ub=[low_bnd], types=self.cplexModel.variables.type.binary)

        # One cables constraints
        for i in range(0, self.nNodes):
            for j in range(0, self.nNodes):
                indexes = [self.yij_index(i, j)] + self.xij_indexes(i, j)
                values = [1] + [-1] * self.nCables
                lhs = cplex.SparsePair(indexes, values)
                rhs = 0
                self.cplexModel.linear_constraints.add(lin_expr=[lhs], senses=['E'], rhs=[rhs], names=[
                    "Sum of X(" + str(i + 1) + "," + str(j + 1) + ",k) equals Y(" + str(i + 1) + "," + str(
                        j + 1) + ")"])

        # Constraints on cable's capacity
        for i in range(0, self.nNodes):
            for j in range(0, self.nNodes):
                indexes = [self.fij_index(i, j)] + self.xij_indexes(i, j)
                values = [-1] + self.capacities()
                lhs = cplex.SparsePair(indexes, values)
                self.cplexModel.linear_constraints.add(lin_expr=[lhs], senses=['G'], names=[
                    "Sum of Capacity(k) times X(" + str(i + 1) + "," + str(j + 1) + ") greater or equal than f(" + str(
                        i + 1) + "," + str(j + 1) + ")"])

        # Soft constraints
        self.sOffset = self.cplexModel.variables.get_num()

        # Capacity soft constraint (thought for just one substation)
        modified_constraints = []
        modified_constraints = self.cplexModel.linear_constraints.get_rows(substationCapacityConstraints)
        cnt = 0
        slack_capacity = []
        for constraint in modified_constraints:
            low_bnd = 0
            up_bnd = self.inf_cplex
            slack_capacity += list(
            self.cplexModel.variables.add(
                lb=[low_bnd], ub=[up_bnd], names=["Capacity soft constraint slack" + str(cnt+1)],
                types=[self.cplexModel.variables.type.integer]))
            constraint.ind += [slack_capacity[cnt]]
            constraint.val += [-1.0]
            self.cplexModel.linear_constraints.set_linear_components(substationCapacityConstraints[cnt],
                                                                     cplex.SparsePair(constraint.ind, constraint.val))
            cnt += 1

        # Flows soft constraint
        modified_constraints = self.cplexModel.linear_constraints.get_rows(flowConstraints)
        cnt = 0
        slack_flow = []
        for constraint in modified_constraints:
            low_bnd = 0
            up_bnd = self.inf_cplex
            slack_flow += \
                list(self.cplexModel.variables.add(lb=[low_bnd], ub=[up_bnd],
                                                   names=["Flow soft constraint slack" + str(cnt+1)],
                                                   types=[self.cplexModel.variables.type.integer]))
            constraint.ind += [slack_flow[cnt]]
            constraint.val += [+1.0]
            self.cplexModel.linear_constraints.set_linear_components(flowConstraints[cnt],
                                                                     cplex.SparsePair(constraint.ind, constraint.val))
            cnt += 1

        # Lazy constraints
        if lazy == 1:  # Insert the lazy constraints
            for x in range(0, self.nNodes):
                p1 = geom_utils.Point(self.modelTurb[x].get_X(), self.modelTurb[x].get_Y())
                for y in range(x + 1, self.nNodes):
                    p2 = geom_utils.Point(self.modelTurb[y].get_X(), self.modelTurb[y].get_Y())
                    for z in range(0, self.nNodes):
                        if z == x or z == y:
                            continue
                        p3 = geom_utils.Point(self.modelTurb[z].get_X(), self.modelTurb[z].get_Y())
                        indexes = [self.yij_index(x, y)] + [self.yij_index(y, x)]
                        values = [1] * len(indexes)
                        for h in range(0, self.nNodes):
                            if h == x or h == y or h == z:
                                continue
                            p4 = geom_utils.Point(self.modelTurb[h].get_X(), self.modelTurb[h].get_Y())
                            if geom_utils.intersects(p1, p2, p3, p4) == 1:
                                indexes += [self.yij_index(z, h)]
                                values += [1]
                        if len(indexes) > 2:
                            lhs = cplex.SparsePair(indexes, values)
                            self.cplexModel.linear_constraints.advanced.add_lazy_constraints(rhs=[1], senses=['L'],
                                                                                             lin_expr=[lhs])
                """ Uncomment to see percentage of completion (MAY CAUSE SLOW DOWN)
                print(x / self.nNodes)
                """

        # Objective function definition
        self.cplexModel.objective.set_sense(self.cplexModel.objective.sense.minimize)
        objective_func = []
        for i in range(0, self.nNodes):
            for j in range(0, self.nNodes):
                for k in range(0, self.nCables):
                    objective_func.append([self.xijk_index(i, j, k), self.price(k) * self.dist(i, j)])


        # Adding slack penalties
        for slack in slack_capacity:
            objective_func.append([slack, 10e8])
        for slack in slack_flow:
            objective_func.append([slack, 10e9])

        # Set objective funcion
        self.cplexModel.objective.set_linear(objective_func)


        # Writes model to file
        #self.cplexModel.write(filename=self.model_folder + "model_prova.lp")

    def nocross_cut(self, a, b, c):
        """ Add no cross constraint between 3 turbines

        :param a: First turbine
        :param b: Second turbine
        :param c: Third Turbine
        :return: 1 if everything goes fine
        """

        indexes = [self.yij_index(a, b)] + [self.yij_index(b, a)]
        values = [1] * len(indexes)
        # Extract coordinates
        p1 = geom_utils.Point(self.modelTurb[a].get_X(), self.modelTurb[a].get_Y())
        p2 = geom_utils.Point(self.modelTurb[b].get_X(), self.modelTurb[b].get_Y())
        p3 = geom_utils.Point(self.modelTurb[c].get_X(), self.modelTurb[c].get_Y())

        # Find the intersection points
        for i in range(0, self.nNodes):
            if i == a or i == b or i == c:
                continue
            p4 = geom_utils.Point(self.modelTurb[i].get_X(), self.modelTurb[i].get_Y())
            if geom_utils.intersects(p1, p2, p3, p4) == 1:
                indexes += [self.yij_index(c, i)]
                values += [1]
        # Add the constraints
        rhs = 1
        lhs = cplex.SparsePair(indexes, values)
        self.cplexModel.linear_constraints.add(rhs=[rhs], senses=['L'], lin_expr=[lhs])

        return 1

    def nocross_cut_callback(self, a, b, c):
        """ Add callback with no cross constraint between 3 turbines

        :param a: First Turbine
        :param b: Second Turbine
        :param c: Third Turbine
        :return: Left expression, Right expression, and operator
        """

        indexes = [self.yij_index(a, b)] + [self.yij_index(b, a)]
        values = [1] * len(indexes)
        # Find the intersection points
        p1 = geom_utils.Point(self.modelTurb[a].get_X(), self.modelTurb[a].get_Y())
        p2 = geom_utils.Point(self.modelTurb[b].get_X(), self.modelTurb[b].get_Y())
        p3 = geom_utils.Point(self.modelTurb[c].get_X(), self.modelTurb[c].get_Y())

        # Find the intersection points
        for i in range(0, self.nNodes):
            if i == a or i == b or i == c:
                continue
            p4 = geom_utils.Point(self.modelTurb[i].get_X(), self.modelTurb[i].get_Y())
            if geom_utils.intersects(p1, p2, p3, p4) == 1:
                indexes += [self.yij_index(c, i)]
                values += [1]

        # Set the expressions
        rhs = 1
        lhs = cplex.SparsePair(indexes, values)

        return [lhs, rhs, 'L']

    def nocross_separation(self, xstar=None):
        """ Looks for intersecting edges in the solution

        :param xstar: Given intersection
        :return: List of (a,b,c,d) which are intersecting in the solution
        """
        epsilon = 0.1

        if xstar is None:
            xstar = self.cplexModel.solution.get_values()

        intersecting = []

        # Check for intersections between all edges
        for i in range(0, self.nNodes):
            p1 = geom_utils.Point(self.modelTurb[i].get_X(), self.modelTurb[i].get_Y())
            for j in range(0, self.nNodes):  # Starts from i+1
                if xstar[self.yij_index(i, j)] < epsilon:
                    continue
                p2 = geom_utils.Point(self.modelTurb[j].get_X(), self.modelTurb[j].get_Y())
                for z in range(0, self.nNodes):
                    if z == i or z == j:
                        continue
                    p3 = geom_utils.Point(self.modelTurb[z].get_X(), self.modelTurb[z].get_Y())
                    for k in range(0, self.nNodes):
                        if k == i or k == j or k == z:
                            continue
                        p4 = geom_utils.Point(self.modelTurb[k].get_X(), self.modelTurb[k].get_Y())
                        if xstar[self.yij_index(z, k)] < epsilon:
                            continue
                        if geom_utils.intersects(p1, p2, p3, p4) == 1: # Intersection exists
                            intersecting.append([i, j, z, k])

        return intersecting

    def nocross_separation_single(self, i, j, xstar=None):
        """ Looks for intersecting edges in the given edge

        :param i: First Turbine
        :param j: Second Turbine
        :param xstar: Given intersection
        :return: List of (a,b,c,d) which are intersecting the edge (i,j)
        """
        epsilon = 0.1

        if xstar is None:
            xstar = self.cplexModel.solution.get_values()

        # Coordinates retrieving
        p1 = geom_utils.Point(self.modelTurb[i].get_X(), self.modelTurb[i].get_Y())
        p2 = geom_utils.Point(self.modelTurb[j].get_X(), self.modelTurb[j].get_Y())

        intersecting = []

        # Check for intersections between all edges
        for z in range(0, self.nNodes):
            if z == i or z == j:
                continue
            p3 = geom_utils.Point(self.modelTurb[z].get_X(), self.modelTurb[z].get_Y())
            for k in range(0, self.nNodes):
                if k == i or k == j or k == z:
                    continue
                p4 = geom_utils.Point(self.modelTurb[k].get_X(), self.modelTurb[k].get_Y())
                if xstar[self.yij_index(z, k)] < epsilon:
                    continue
                if geom_utils.intersects(p1, p2, p3, p4) == 1:  # Intersection exists
                    intersecting.append([i, j, z, k])

        return intersecting

    def solve_model(self):
        """ Calls the methods to solve the instance

        :return: 0 if allright, negative number otherwise
        """
        self.cplexModel.solve()

        try:
            self.cplexModel.solution.get_values()
        except cplex.exceptions.CplexSolverError:
            print("No solution yet")
            return [None, None]

        return [self.cplexModel.solution.get_values(), self.cplexModel.solution.get_objective_value()]

    def solveWithTime(self, timer):
        """ Solve the model with a given time

        :param timer: Time of execution
        :return: Solution values
        """
        # Save the previous one
        prec = self.timeLimit

        # Set time limit and solve
        self.cplexModel.parameters.timelimit.set(float(timer))
        self.cplexModel.solve()

        # No solution case
        try:
            self.cplexModel.solution.get_values()
        except cplex.exceptions.CplexSolverError:
            print("No solution yet")
            return [None, None]

        # Restore the previous time
        self.cplexModel.parameters.timelimit.set(float(prec))
        return [self.cplexModel.solution.get_values(), self.cplexModel.solution.MIP.get_best_objective()]

    def update_best(self, solution, z):
        """ If the solution is better than the last one saved we update it

        :param solution: values of the solution offered by cplex
        :param z: objective function of the solution
        :return: 1 if the solution gets updated, 0 if it doesn't
        """
        if z < self.bestZ:
            self.bestZ = z
            self.bestSol = solution
            return 1
        else:
            return 0

    def print_solution(self, tStart):
        """ Prints the solution in text

        :param tStart: starting time of the optimization
        :return: 0 if allright, negative number otherwise
        """
        sol = self.cplexModel.solution.get_values()
        print("------ Solution ------")
        for i in range(0, self.nNodes):
            for j in range(0, self.nNodes):
                if sol[self.yij_index(i, j)] >= 0.5:  # If the cable is connected
                    k_indexes = self.xij_indexes(i, j)
                    for k in k_indexes:
                        if sol[k] >= 0.5:
                            print("Cable from station " + str(i + 1) + " to station " + str(j + 1) + " of type " + str(
                                self.get_k_from_index(k) + 1) + " supporting a flow of " + str(
                                sol[self.fij_index(i, j)]))

        print("Solution obtained in " + str(time.time() - tStart) + " seconds")
        return 0

    def draw_solution(self, sol, iteration=None, solution_cost=None, multiFigure=True, intersections=True, printCrosses=True, save_fig=False):
        """ Draws graphically the solution obtained

        :param sol: solution to draw
        :param iteration: iteration number
        :param solution_cost: cost of the actual solution
        :param multiFigure: if True creates multiple figures for the iterations, with False
                            updates the older one
        :param intersections: if True draw the intersections, False otherwise
        :return: 0 if allright, negative number otherwise
        """

        # Set single figure
        if multiFigure is True:
            if iteration is None:
                plt.figure(1, figsize=(9, 6))
            else:
                plt.figure(iteration, figsize=(9, 6))

        # Check for crossing
        crosses = self.nocross_separation(sol)
        if printCrosses == True:
            if len(crosses) == 0:
                print("No crossing")
            else:
                print("There are crossing")
                print(crosses)

        plt.ion()
        plt.clf()
        ax = plt.subplot(111)

        x = []
        y = []
        labels = []
        node_colors = []

        # Set turbines
        for i in range(0, self.nNodes):
            x.append(self.modelTurb[i].get_X())
            y.append(self.modelTurb[i].get_Y())
            labels += [str(i)]
            if self.modelTurb[i].get_T() == -1:
                node_colors.append('r')
            else:
                node_colors.append('b')
        x = np.array(x)
        y = np.array(y)
        node_colors = np.array(node_colors)

        for label, x_f, y_f in zip(labels, x, y):
            plt.annotate(label, xy=(x_f, y_f), xytext=(x_f-250, y_f+50))

        # Legend creation
        legend_box = []

        # Adding the nodes legend
        legend_box.append(mlines.Line2D([], [], color='blue', marker='o',
                                        linestyle='None', label='Turbine'))  # Turbine Node
        legend_box.append(mlines.Line2D([], [], color='red', marker='o',
                                        linestyle='None', label='Substation'))  # Substation Node
        legend_box.append(mlines.Line2D([], [], color='white', linestyle='None'))

        # Adding the cables legend, with variable number of cables
        color_palette = self.generate_color_palette(len(self.modelCables))
        for i in range(0, len(color_palette)):
            line_label = "Cable " + str(i) + ", c=" + str(self.modelCables[i].get_capacity())
            legend_box.append(mlines.Line2D([], [], color=color_palette[i], label=line_label))
        legend_box.append(mlines.Line2D([], [], color='white', linestyle='None'))

        # Adding the intersections legend
        legend_box.append(mlines.Line2D([], [], color='black', marker='*', linestyle='None', label='Intersection'))

        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        # Put a legend to the right of the current axis
        plt.legend(handles=legend_box, loc='center left', bbox_to_anchor=(1, 0.5))

        # Set axis
        plt.scatter(x, y, c=node_colors, zorder=0)
        plt.xlabel("x")
        plt.ylabel("y")

        # Set loop cycle number
        title_string = ""
        if iteration is not None:
            title_string += "Loop Iteration " + str(iteration) + "\n"
        if solution_cost is not None:
            title_string += "Solution Cost " + str(solution_cost)
        plt.title(title_string)

        # Draws the connections
        for i in range(0, self.nNodes):
            for j in range(0, self.nNodes):
                # If the cable is connected
                if sol[self.yij_index(i, j)] >= 0.5:
                    k_indexes = self.xij_indexes(i, j)
                    base_value = k_indexes[0]
                    for k in k_indexes:
                        if sol[k] >= 0.5:
                            dx = x[j] - x[i]
                            dy = y[j] - y[i]
                            plt.arrow(x[i], y[i], dx, dy, head_width=100, color=color_palette[k-base_value], zorder=1)

        # DRAWS THE INTERSECTIONS
        if intersections:
            crosses = self.nocross_separation(sol)
            int_points_x = []
            int_points_y = []
            for cross in crosses:
                p1 = geom_utils.Point(self.modelTurb[cross[0]].get_X(), self.modelTurb[cross[0]].get_Y())
                p2 = geom_utils.Point(self.modelTurb[cross[1]].get_X(), self.modelTurb[cross[1]].get_Y())
                p3 = geom_utils.Point(self.modelTurb[cross[2]].get_X(), self.modelTurb[cross[2]].get_Y())
                p4 = geom_utils.Point(self.modelTurb[cross[3]].get_X(), self.modelTurb[cross[3]].get_Y())
                intersection_point = geom_utils.intersection_point(p1, p2, p3, p4)
                int_points_x.append(intersection_point.x)
                int_points_y.append(intersection_point.y)
            int_points_x = np.array(int_points_x)
            int_points_y = np.array(int_points_y)
            plt.scatter(int_points_x, int_points_y, marker='*', color='black', zorder=10)

        plt.pause(0.001)

        if save_fig:
            plt.savefig("../figures/Figure1.png")
            print("woo")

        return 0

    @staticmethod
    def generate_color_palette(n):
        """ Generates n colors evenly spread in the spectrum, using seaborn library

        :param n: number of colors
        :return: list of colors (in RGB)
        """
        return sns.color_palette("hls", n)

    def read_command_line(self):
        """ Sets parameters given from command line

        :return 0 if all right, negative number otherwise
        """
        argv = sys.argv
        hp = 0
        for i in range(1, len(argv), 2):
            if argv[i] == "-fileT":
                self.turbFile = str(argv[i + 1])
                continue
            elif argv[i] == "-fileC":
                self.cableFile = str(argv[i + 1])
                continue
            elif argv[i] == "-time_limit":
                self.timeLimit = float(argv[i + 1])
                continue
            elif argv[i] == "-timeout":
                self.timeout = float(argv[i + 1])
                continue
            elif argv[i] == "-substCap":
                self.substationCapacity = int(argv[i + 1])
                continue
            elif argv[i] == "-rins":
                self.rins = int(argv[i + 1])
                continue
            elif argv[i] == "-polish":
                self.polish = int(argv[i + 1])
                continue
            elif argv[i] == "-tenure":
                self.tenure = int(argv[i + 1])
                continue
            elif argv[i] == "-initialization_method":
                self.initialization_method = int(argv[i + 1])
                continue
            elif argv[i] == "-choice":
                self.choice = int(argv[i + 1])
                if self.choice == 7:  # For taboo search case introduce dummy cables
                    self.dummy = True
                else:
                    self.dummy = False
                continue
            elif argv[i] == "-help" or argv[i] == "--help":
                hp = 1
                continue
            else:
                hp = 1
                break  # default action

        if hp == 1:
            print("\n\navailable parameters (vers. 28-mar-2018) --------------------------------------------------\n")
            print("-fileT \n" + str(self.turbFile))
            print("-fileC \n" + str(self.cableFile))
            print("-time_limit \n" + str(self.timeLimit))
            print("-substCap \n" + str(self.substationCapacity))
            print("-rins \n" + str(self.rins))
            print("-polish \n" + str(self.polish))
            print("-tenure \n" + str(self.tenure))
            print("-initialization_method \n" + str(self.initialization_method))
            print("-choice \n" + str(self.choice))
            print("\nenter -help or --help for help\n")
            print(
                "----------------------------------------------------------------------------------------------\n\n")

    # DEFINITION OF SUPPORT FUNCTIONS #
    def yij_index(self, i, j):
        """ Return the index of yij

        :param i: i index
        :param j: j index
        :return: index of the variable Yij
        """
        return self.yOffset + i * self.nNodes + j

    def fij_index(self, i, j):
        """ Return the index of fij

        :param i: i index
        :param j: j index
        :return: index of the variable Fij
        """
        return self.fOffset + i * self.nNodes + j

    def xijk_index(self, i, j, k):
        """ Return the index of xijk

        :param i: i index
        :param j: j index
        :param k: k index
        :return: index of the the variable Xijk
        """
        return self.xOffset + self.nCables * (self.nNodes * i) + self.nCables * j + k

    def out_indexes_y(self, h):
        """ Returns the locations of the out degree indexes of Yhj, with h fixed

        :param h: index of the fixed node
        :return: list of indexes
        """
        indexes = list(range(self.yOffset + h * self.nNodes, self.yOffset + (h + 1) * self.nNodes))
        return indexes

    def in_indexes_y(self, h):
        """ Returns the locations of the in degree indexes of Yih, with h fixed

        :param h: Index of the fixed node
        :return: List of indexes
        """
        indexes = list(range(self.yOffset + h, self.yOffset + self.nNodes * self.nNodes, self.nNodes))
        return indexes

    def out_indexes_f(self, h):
        """ Returns the locations of the out degree indexes of fhj, with h fixed

        :param h: Index of the fixed node
        :return: List of indexes
        """
        indexes = list(range(self.fOffset + h * self.nNodes, self.fOffset + (h + 1) * self.nNodes))
        return indexes

    def in_indexes_f(self, h):
        """ Returns the locations of the in degree indexes of fih, with h fixed

        :param h: Index of the fixed node
        :return: List of indexes
        """
        indexes = list(range(self.fOffset + h, self.fOffset + self.nNodes * self.nNodes, self.nNodes))
        return indexes

    def xij_indexes(self, i, j):
        """ Returns list of the indices of the variables Xijk, with i,j fixed

        :param i: i index
        :param j: j index
        :return: List of indexes
        """
        startingIndex = self.xOffset + self.nCables * (self.nNodes * i) + self.nCables * j

        indexes = list(range(startingIndex, startingIndex + self.nCables, 1))

        return indexes

    def get_k_from_index(self, index):
        """ Returns the tipe k of a cable given an edge

        :param index: Index of the solution vector
        :return: The type k of the cable
        """
        return (index - self.xOffset) % self.nCables

    def capacity(self, k):
        """ Returns the capacity of a cable k

        :param k: Index of the cable
        :return: Capacity of the cable
        """
        return self.modelCables[k].get_capacity()

    def capacities(self):
        """ Returns a list with all the capacities of the cables

        :return: List of capacities
        """
        cap = []
        for i in range(0, len(self.modelCables)):
            cap = cap + [self.capacity(i)]
        return cap

    def price(self, k):
        """ Returns the price of a cable k

        :param k: Index of the cable
        :return: Price of the cable
        """
        return self.modelCables[k].get_price()

    def dist(self, i, j):
        """ Returns the distance between 2 stations

        :param i: index of the first station
        :param j: index of the second station
        :return: distance between them
        """
        return self.modelTurb[i].dist(self.modelTurb[j])

    # We suppose to be using the dummy cable
    def calculate_cost_modified(self, sol, i, j, succ):
        """ Parametric cost calculation for heuristics, may change sol and succ values

        :param sol: The solution vector
        :param i: First Turbine
        :param j: Second Turbine
        :param succ: tree representation
        :return: delta in cost induced from the i,j move
        """

        delta = 0

        # removing old cable
        for k in range(0, self.nCables):
            delta -= sol[self.xijk_index(i, succ[i], k)] * self.price(k) * self.dist(i, succ[i])
            sol[self.xijk_index(i, succ[i], k)] = 0
        deducted_flow = sol[self.fij_index(i, succ[i])]
        sol[self.fij_index(i, succ[i])] = 0
        sol[self.yij_index(i, succ[i])] = 0

        # Changing the following cables to better ones
        l = succ[i]
        while succ[l] != l:
            sol[self.fij_index(l, succ[l])] -= deducted_flow
            tmp_flow = sol[self.fij_index(l, succ[l])]

            dist = self.dist(l, succ[l])
            for k in range(0, self.nCables):
                delta -= sol[self.xijk_index(l, succ[l], k)] * self.price(k) * dist
                sol[self.xijk_index(l, succ[l], k)] = 0

            tmp_cable = -1
            for k in range(0, self.nCables):
                if self.capacity(k) >= tmp_flow:
                    if tmp_cable == -1 or self.price(k) < self.price(tmp_cable):
                        tmp_cable = k
            sol[self.xijk_index(l, succ[l], tmp_cable)] = 1
            delta += dist * self.price(tmp_cable)
            l = succ[l]

        # Adding the new cable
        succ[i] = j
        l = i
        while succ[l] != l:
            sol[self.fij_index(l, succ[l])] += deducted_flow
            tmp_flow = sol[self.fij_index(l, succ[l])]

            dist = self.dist(l, succ[l])
            for k in range(0, self.nCables):
                delta -= sol[self.xijk_index(l, succ[l], k)] * self.price(k) * dist
                sol[self.xijk_index(l, succ[l], k)] = 0

            tmp_cable = -1
            for k in range(0, self.nCables):
                if self.capacity(k) >= tmp_flow:
                    if tmp_cable == -1 or self.price(k) < self.price(tmp_cable):
                        tmp_cable = k
            sol[self.xijk_index(l, succ[l], tmp_cable)] = 1
            delta += dist * self.price(tmp_cable)
            l = succ[l]

        return delta

    def update_sol(self, sol, i, j, succ):
        """ Makes the 1-opt move, updates the structures

        :param sol: cplex-like solution vector
        :param i: starting node
        :param j: end node
        :param succ: tree representation
        :return: updated sol and succ
        """
        for k in range(0, self.nCables):
            sol[self.xijk_index(i, succ[i], k)] = 0
        deducted_flow = sol[self.fij_index(i, succ[i])]
        sol[self.fij_index(i, succ[i])] = 0
        sol[self.yij_index(i, succ[i])] = 0

        l = succ[i]
        while succ[l] != l:  # While I'm not on the substation
            sol[self.fij_index(l, succ[l])] -= deducted_flow
            tmp_flow = sol[self.fij_index(l, succ[l])]

            for k in range(0, self.nCables):
                sol[self.xijk_index(l, succ[l], k)] = 0

            tmp_cable = -1
            for k in range(0, self.nCables):
                if self.capacity(k) >= tmp_flow:
                    if tmp_cable == -1 or self.price(k) < self.price(tmp_cable):
                        tmp_cable = k
            sol[self.xijk_index(l, succ[l], tmp_cable)] = 1
            l = succ[l]

        sol[self.yij_index(i, j)] = 1
        succ[i] = j
        l = i

        while succ[l] != l:  # While I'm not on the substation
            sol[self.fij_index(l, succ[l])] += deducted_flow
            tmp_flow = sol[self.fij_index(l, succ[l])]
            tmp_cable = -1
            for k in range(0, self.nCables):
                sol[self.xijk_index(l, succ[l], k)] = 0

            for k in range(0, self.nCables):
                if self.capacity(k) >= tmp_flow:
                    if tmp_cable == -1 or self.price(k) < self.price(tmp_cable):
                        tmp_cable = k
            sol[self.xijk_index(l, succ[l], tmp_cable)] = 1
            l = succ[l]

        return [sol, succ]


class NoCrossCallback(cplex.callbacks.UserCutCallback):
    def __init__(self, env):
        cplex.callbacks.UserCutCallback.__init__(self, env)
        self.model = None
        self.total_time = 0
        self.old_sol = None

    def __call__(self, *args, **kwargs):
        assert isinstance(self.model, CPLXModel)
        start_t = time.time()
        # value = self.get_incumbent_objective_value()
        #if self.model.bestZ <= self.get_incumbent_objective_value():
        #    self.total_time += time.time() - start_t
        #    return
        #
        if self.get_incumbent_values() == self.old_sol:
            self.total_time += time.time() - start_t
            return

        indices = self.model.nocross_separation(xstar=self.get_incumbent_values())
        if len(indices) > 0:
            print("Inserting " + str(len(indices)) + " cuts")
            for cut in indices:
                [lhs, rhs, sense] = self.model.nocross_cut_callback(cut[0], cut[1], cut[2])
                self.add(cut=lhs, sense="L", rhs=float(rhs))
                # PROVA, CANCELLARE SOTTO (Aggungiamo entrambi i lati così)
                [lhs, rhs, sense] = self.model.nocross_cut_callback(cut[2], cut[3], cut[0])
                self.add(cut=lhs, sense="L", rhs=float(rhs))
        else:

            self.model.update_best(self.get_incumbent_values(), self.get_incumbent_objective_value())
            print("DING DING DING")

        self.old_sol = self.get_incumbent_values()
        self.total_time += time.time() - start_t

        print(self.total_time)
        return




        # print("Into the callback, num: " + str(len(self.get_values(*args))) + ", values: " + str(self.get_values(*args)))

# Class for Lazy constraints callback #
class NoCrossLazyCallback(cplex.callbacks.LazyConstraintCallback):

    def __init__(self, env):  # Constructor
        cplex.callbacks.LazyConstraintCallback.__init__(self, env)
        self.model = None
        self.counter = 0


    def __call__(self, *args, **kwargs):
        assert isinstance(self.model, CPLXModel)
        try:
            value = self.get_incumbent_objective_value()
            self.get_incumbent_values()
        except:
            return
        self.counter += 1
        print(str(self.counter))
        #if self.model.bestZ <= self.get_incumbent_objective_value():
        #    return
        self.model.update_best(self.get_incumbent_values(), self.get_incumbent_objective_value())

        indices = self.model.nocross_separation(xstar=self.get_incumbent_values())
        if len(indices) > 0:
            print("Inserting " + str(len(indices)) + " cuts")

        for cut in indices:
            [lhs, rhs, sense] = self.model.nocross_cut_callback(cut[0], cut[1], cut[2])
            self.add(constraint=lhs, sense="L", rhs=float(rhs))
            # PROVA, CANCELLARE SOTTO (Aggungiamo entrambi i lati così)
            [lhs, rhs, sense] = self.model.nocross_cut_callback(cut[2], cut[3], cut[0])
            self.add(constraint=lhs, sense="L", rhs=float(rhs))
        #print("Into the callback, num: " + str(len(self.get_values(*args))) + ", values: " # + str(self.get_values(*args)))
