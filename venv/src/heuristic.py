"""
 * @file turbines.py
 * @brief Implementation of heuristic methods.
 *
 * @author Federico Turrin (<federico.turrin@studenti.unipd.it>)
 * @author Marco Soldan (<marco.soldan@studenti.unipd.it>)
 * @version 1.0
 * @since 1.0
 *
 * @copyright Copyright (c) 2017-2018 University of Padua, Italy
 * @copyright Apache License, Version 2.0
"""

import cplex
import time
import random
import matplotlib.pyplot as plt
import numpy as np
from operator import itemgetter


# Solves the model with hardfixing heuristic
def hardFixing(cM, iterationTime):
    """ Heuristic solution using hardfixing algorithm

    :param cM: The model
    :param iterationTime: Time for a single iteration
    :return: Nothing
    """

    # First solution to set the model
    print("Initial dummy solution")
    [solution, cost] = cM.solveWithTime(timer=5)  # First dummy solution

    # Variables definition
    timeout = time.time() + cM.timeLimit
    cnt = 0  # Loop counter
    vlist = []  # Save edge modification

    gap = 100
    perc = 100

    while (cM.cplexModel.solution.get_status() is not cM.cplexModel.solution.status.optimal
           and cM.cplexModel.solution.get_status() is not cM.cplexModel.solution.status.optimal_tolerance) \
            and time.time() < timeout:

        cnt += 1

        # Random seed generation
        random.seed(random.SystemRandom)

        # Randomly fix variables
        for i in range(0, cM.nNodes*cM.nNodes):
            if solution[i] == 1 and random.randint(0, 100) >= perc:  # If num in [0,perc] fix the bound
                cM.cplexModel.variables.set_lower_bounds(i, 1)
                vlist.append(i)  # Append the modified edges

        print("*******Heuristic solving*******")
        [solution, z] = cM.solveWithTime(timer=iterationTime)  # Solve with fixed cables

        cM.draw_solution(solution, iteration=cnt, solution_cost=z, multiFigure=False)  # Print the solution

        # Unfix bound
        for i in vlist:
            cM.cplexModel.variables.set_lower_bounds(i, 0)

        vlist.clear()  # Clear the modified edges list

        if len(cM.nocross_separation(xstar=solution)) == 0:
            cM.update_best(solution, z)
        cM.solveWithTime(timer=5)  # Solve with cplex for updating the real gap

        # If gap is lower -> increase probability
        if (cM.cplexModel.solution.get_status() != 0
                and 100 * cM.cplexModel.solution.MIP.get_mip_relative_gap() < gap):
            gap = 100 * cM.cplexModel.solution.MIP.get_mip_relative_gap()
            print("******MIGLIORATA******" + " (" + str(gap) + ")")
            perc -= 5
            perc = max(10, perc)


        else:  # Else reduce the probability
            perc += 5
            perc = min(90, perc)
            print("******NON MIGLIORATA******" + " (" + str(gap) + ")")


# Solves the model with softfixing heuristic
def softFixing(cM, iterationTime):
    """ Heuristic solution using softfixing algorithm

    :param cM: The model
    :param iterationTime: Time for a single iteration
    :return: Nothing
    """

    cM.cplexModel.parameters.mip.limits.solutions.set(1)
    cM.solveWithTime(timer=600)  # first dummy solution
    solution = cM.cplexModel.solution.get_values()
    cM.update_best(solution, cM.cplexModel.solution.get_objective_value())
    # Variables definition
    cM.cplexModel.parameters.timelimit.set(float(iterationTime))
    k = cM.nNodes - 1

    cnt = 0
    gap = 0.9
    timeout = time.time() + cM.timeout

    indexes = []
    for i in range(0, cM.nNodes):
        for j in range(0, cM.nNodes):
            if solution[cM.yij_index(i, j)] > 0.5:
                indexes.append(cM.yij_index(i, j))
    values = [1] * len(indexes)

    lhs = cplex.SparsePair(indexes, values)
    rhs = len(indexes) - k
    softConstraint = cM.cplexModel.linear_constraints.add(lin_expr=[lhs], rhs=[rhs], senses=['G'])

    while (cM.cplexModel.solution.get_status() is not cM.cplexModel.solution.status.optimal
           and cM.cplexModel.solution.get_status() is not cM.cplexModel.solution.status.optimal_tolerance) \
            and time.time() < timeout:


        cnt += 1
        indexes = []
        for i in range(0, cM.nNodes):
            for j in range(0, cM.nNodes):
                if solution[cM.yij_index(i, j)] > 0.5:
                    indexes.append(cM.yij_index(i, j))
        values = [1] * len(indexes)

        lhs = cplex.SparsePair(indexes, values)
        rhs = len(indexes) - k
        #cM.cplexModel.linear_constraints.add(lin_expr=[lhs], rhs=[rhs], senses=['G'])
        cM.cplexModel.linear_constraints.set_linear_components(softConstraint[0], lhs)
        cM.cplexModel.linear_constraints.set_rhs(softConstraint[0], rhs)
        print("CONSTRAINTS +1: " + str(cM.cplexModel.linear_constraints.get_num()))

        print("********Heuristic solving************")
        #[solution, z] = cM.solveWithTime(timer=iterationTime)  # Solving with new constraints
        cM.cplexModel.parameters.mip.limits.solutions.set(cnt+1)
        cM.cplexModel.parameters.timelimit.set(float(timeout - time.time()))
        cM.cplexModel.solve()
        try:
            cM.cplexModel.solution.get_values()
            tmp_gap = cM.cplexModel.solution.MIP.get_mip_relative_gap()
        except cplex.exceptions.CplexSolverError:
            continue
        solution = cM.bestSol
        #solution = cM.cplexModel.solution.get_values()
        # Print the solution
        cM.draw_solution(solution, iteration=cnt, multiFigure=False)
        # If gap is better -> increase k

        print("GAP CHECK: " + str(tmp_gap))

        if (tmp_gap < gap and len(cM.nocross_separation(xstar=cM.bestSol)) == 0):
            gap = tmp_gap
            k -= 5
            k = max(5, k)
            print("******BETTER SOLUTION******" + " (" + str(gap) + ")" + ", k modified to: " + str(k))
        else:
            print("******SOLUTION NOT BETTER******" + " (" + str(gap) + ")" + ", k modified to: " + str(k))
            k += 5
            k = min(cM.nNodes-1, k)


def greedy(cM, timer, once=False, initialization_method=1):
    """ Searches for an optimal solution by creating various different trees (with grasp),
        also used as auxiliary function by taboo_search to initialize the tree

    :param cM: Model on which the tree is based
    :param timer: time it has to run before stopping
    :param once: by default False, if True executes only once (used by taboo_search)
    :param initialization_method: takes values 1 (GRASP) o 2 (Kruskaal modified)
    :return: the best solution obtained, by form of successive vector and of simil-cplex solution vector
    """

    timeout = time.time() + timer

    while time.time() < timeout:

        solu = [0]*(cM.nNodes*cM.nNodes+cM.nNodes*cM.nNodes+cM.nNodes*cM.nNodes*len(cM.capacities())+cM.nNodes)

        if initialization_method == 1:
            succ = dijkstra_grasp(cM)
        elif initialization_method == 2:
            succ = kruskaal_M(cM)

        cM.yOffset = 0
        cM.fOffset = cM.nNodes*cM.nNodes
        cM.xOffset = cM.fOffset + cM.nNodes*cM.nNodes
        cM.sOffset = cM.xOffset + cM.nNodes*cM.nNodes*len(cM.capacities())
        for i in range(1, cM.nNodes):
            solu[cM.yij_index(i, succ[i])] = 1
        pred = [[] for _ in range(cM.nNodes)]
        for i in range(0, cM.nNodes):
                pred[succ[i]].append(i)
        #for i in range(0, cM.nNodes):
        #    print("Il predecessore di "+str(i)+"e' "+str(pred[i]))

        setCable(succ, pred, cM, solu)
        cM.draw_solution(sol=solu, intersections=False)
        if once is True:
            return [solu, succ]
        succ.clear()
        solu.clear()


def dijkstra_grasp(cM, choice=5):
    """ Implementation of Dijkstra tree generation algorithm by using grasp method

    :param cM: Model on which the tree is based
    :param choice: Number of solutions to choose from
    :return: The tree represented by a list of successors
    """
    flag = []
    L = []
    succ = []
    setC = set()

    # Root configuration
    flag.append(1)
    succ.append(0)
    L.append(0)

    # Tree initialization
    for j in range(1, cM.nNodes):
        flag.append(0)
        L.append(cM.dist(0, j))  # Substation research
        succ.append(0)

    # Fix the firsts C station
    for i in range(0, cM.substationCapacity):
        minim = 10e12
        h = - 1
        for j in range(1, cM.nNodes):
            if flag[j] == 0 and cM.dist(0, j) < minim:
                h = j
                minim = cM.dist(0, j)
        setC.add(h)
        flag[h] = 1
    for j in range(1, cM.nNodes):  # Update
        for h in setC:
            if flag[j] == 0 and cM.dist(h, j) < L[j]:
                L[j] = cM.dist(h, j)
                succ[j] = h

    # Fix the other n-C-1 turbines
    for k in range(0, cM.nNodes-1-cM.substationCapacity):
        minim = 10e12
        li = []
        if random.randint(0, 1) == 0:  # Do the optimal choice
            opt = 0
        else:  # Chose between one of the five better choices
            opt = random.randint(0, choice-1)

        for j in range(1, cM.nNodes):
            if flag[j] == 0 and L[j] < minim:  # Look for a minimum not yet visited
                li.append([L[j], j])
                li.sort(key=itemgetter(0))
                if len(li) > choice:
                    leng = len(li)
                    temp = li[leng-1]
                    minim = temp[0]
                    li.remove(temp)
                else:
                    minim = L[j]

        if minim == 10e12:  # No minimum found
            continue

        # I take the one randomly choosed
        while opt > len(li) - 1:  # Not enough optimum case
            opt = opt-1
        h = (li[opt])[1]
        flag[h] = 1
        for j in range(1, cM.nNodes):
            if flag[j] == 0 and cM.dist(h, j) < L[j]:
                L[j] = cM.dist(h, j)
                succ[j] = h
        li.clear()

    """ Uncomment to print the list
    for i in range(0, len(succ)):
        #print("il pred di "+str(i)+" e' "+str(succ[i]))
    """
    return succ


def kruskaal(cM):
    """ Create mimimum spanning tree with kruskaal algorithm

    :param cM: The model used
    :return: The tree represented by a list of successors
    """
    costs = []
    succ = [0]*cM.nNodes
    setC = set()
    setC.add(0)

    # Edges sorting
    for i in range(0, cM.nNodes):
        for j in range(0, cM.nNodes):
            costs.append([i, j, cM.dist(i, j)])

    costs.sort(key=lambda tup: tup[2])  # Sort by increasing cost


    # Fix the firsts C station
    rim = 0
    for i in range(0, cM.substationCapacity):
        min=10e8
        for j in range(0, len(costs)):
            if costs[j][0] == 0 and costs[j][2] < min and (costs[j][1] not in setC):  # If the is the substation
                min = costs[j][2]
                rim=costs[j][1]
        setC.add(rim)

    k = 0
    h = 0
    comp = []


    # Initialize components
    for i in range(0, cM.nNodes):
        comp.append(i)

    while (k < cM.nNodes) and (h < len(costs) - 1):
        h += 1
        i = costs[h][0]
        if i in setC:
            continue
        j = costs[h][1]
        C1 = comp[i]
        C2 = comp[j]
        if C1 != C2:  # Choose (i, j)
            k = k + 1
            succ[i] = j
            setC.add(i)
            for q in range(1, cM.nNodes):  # Merge components C1 and C2
                if comp[q] == C2:
                    comp[q] = C1

    if k != cM.nNodes - 1:
        print("GRAPH NOT CONNECTED")
    return succ

def kruskaal_M(cM):
    """ Create mimimum spanning tree with kruskaal algorithm modified

       :param cM: The model used
       :return: The tree represented by a list of successors
       """
    maxSubstationConnection = cM.substationCapacity
    print("subCap:")
    print(cM.substationCapacity)
    costs = []
    succ = [0] * cM.nNodes
    setC = set()
    setC.add(0)

    maxC = 0
    for c in cM.modelCables:
        if c.get_capacity() < 1e3 and c.get_capacity() > maxC:
            maxC = c.get_capacity()

    # Edges sorting
    for i in range(0, cM.nNodes):
        for j in range(0, cM.nNodes):
            costs.append([i, j, cM.dist(i, j)])

    costs.sort(key=lambda tup: tup[2])  # Sort by increasing cost

    k = 0
    h = 0
    comp = []

    # Initialize components
    for i in range(0, cM.nNodes):
        comp.append(i)

    substationConnections = 0
    while (k < cM.nNodes) and (h < len(costs) - 1):
        h += 1
        i = costs[h][0]
        if i in setC:
            continue
        j = costs[h][1]
        C1 = comp[i]
        C2 = comp[j]
        if j == 0 and substationConnections == maxSubstationConnection:
            continue
        elif j == 0:
            substationConnections += 1

        if C1 != C2:  # Choose (i, j)
            tot = 0
            for l in range(0, cM.nNodes):
                if comp[l] == C1 or comp[l] == C2:
                    tot += 1
            if tot < maxC:
                k = k + 1
                succ[i] = j
                setC.add(i)
                for q in range(1, cM.nNodes):  # Merge components C1 and C2
                    if comp[q] == C2:
                        comp[q] = C1

    return succ


def setCable(succ, pred, cM, solu):
    """

    :param succ: Tree composed by a list of successors
    :param pred: Tree represented by a list of predecessors
    :param cM: The model used
    :param solu: The solution array
    :return: Nothing
    """

    leaf = []  # Will contain the leafs
    cables = cM.capacities()  # List with cables capacity
    enterflow = [0] * cM.nNodes

    # Leafs computation
    for i in range(0, cM.nNodes):
        if len(pred[i]) == 0:  # Elements at 0 are leafs
            leaf.append(i)

    # FLow setting
    for i in range(0, len(leaf)):
        node = leaf[i]
        while node != 0:
            solu[cM.fij_index(node, succ[node])] = 1 + enterflow[node]  # Increase flow on cable
            enterflow[succ[node]] = 0
            for j in pred[succ[node]]:
                enterflow[succ[node]] += solu[cM.fij_index(j, succ[j])]
            node = succ[node]

    # Cables setting
    for i in range(1, cM.nNodes):
        cap = solu[cM.fij_index(i, succ[i])]
        for j in range(0, len(cables)):
            if cap <= cables[j]:  # I can use the cable with capacity j
                solu[cM.xijk_index(i, succ[i], j)] = 1
                break
        if cap > cables[len(cables)-1]:  # Flow loss
            solu[cM.xijk_index(i, succ[i], len(cables)-1)] = 1  # Use the bigger cable
            print("Flow loss in cable (" + str(i) + "," + str(succ[i]) + ")")
    return


def print_tree(cM, sol):
    """ Print the tree

    :param cM: The model used
    :param sol: The solution vector
    :return: Nothing
    """
    # Set one plot at time
    multiFigure = True
    iteration = None
    if multiFigure is True:
        if iteration is None:
            plt.figure(1, figsize=(9, 6))
        else:
            plt.figure(iteration, figsize=(9, 6))

    plt.ion()
    plt.clf()
    plt.subplot(111)

    x = []
    y = []
    labels = []
    node_colors = []

    # Set up the turbines position
    for i in range(0, cM.nNodes):
        x.append(cM.modelTurb[i].get_X())
        y.append(cM.modelTurb[i].get_Y())
        labels += [str(i)]
        if cM.modelTurb[i].get_T() == -1:
            node_colors.append('r')
        else:
            node_colors.append('b')
    x = np.array(x)
    y = np.array(y)
    node_colors = np.array(node_colors)

    # Set up the turbine label
    for label, x_f, y_f in zip(labels, x, y):
        plt.annotate(label, xy=(x_f, y_f), xytext=(x_f - 250, y_f + 50))

    # Set up the plot
    plt.scatter(x, y, c=node_colors, zorder=0)
    plt.xlabel("x")
    plt.ylabel("y")

    # Insert the loop number
    title_string = ""
    if iteration is not None:
        title_string += "Loop Iteration " + str(iteration) + "\n"
    plt.title(title_string)

    # Draws the connections
    for i in range(0, cM.nNodes):
        for j in range(0, cM.nNodes):
            # If the cable is connected
            if sol[cM.yij_index(i, j)] >= 0.5:
                dx = x[j] - x[i]
                dy = y[j] - y[i]
                plt.arrow(x[i], y[i], dx, dy, head_width=100, color="blue", zorder=1)
    plt.pause(0.001)
    return 0

def taboo_search(cM, substation_cables_capacity, initialization_method, tenure, timeo):
    """ Taboo_search algorithm to find an optimal solution

    :param cM: Model on which the solution is based
    :param substation_cables_capacity: maximum number of cables the solution can substain
    :param initialization_method: 1 to start from a GRASP solution, 2 to start from a modified Kruskaal solution
    :param tenure: parameter which describes how much the move space is costrained (the higher, the more)
    :param time: number of seconds the program will run
    :return:
    """
    swaps = 0
    taboo_list = []
    substation_cables = 0
    iteration = 0

    # To obtain the starting solution through dijkstra grasp
    if initialization_method == 1:
        [sol, succ] = greedy(cM, 100, once=True, initialization_method=1)
    elif initialization_method == 2:
        [sol, succ] = greedy(cM, 100, once=True, initialization_method=2)
    else:
        print("Error in initialization of taboo_search")
        return

    cost = 0
    numC = 0

    # find the substation node
    substation = -1
    for i in range(0, cM.nNodes):
        tmp = -1
        for j in range(0, cM.nNodes):
            if sol[cM.yij_index(i, j)] == 1:
                tmp = i
        if tmp is -1:
            substation = i

    for i in range(0, cM.nNodes):
        for j in range(0, cM.nNodes):
            for k in range(0, cM.nCables):
                numC += sol[cM.xijk_index(i, j, k)]
                cost += sol[cM.xijk_index(i, j, k)] * cM.price(k) * cM.dist(i, j)
                if j == substation and sol[cM.xijk_index(i, j, k)] == 1:
                    substation_cables += 1

    print("Substation cables : " + str(substation_cables))

    crosses = cM.nocross_separation(sol)
    for cross in crosses:
        cross1 = [cross[2], cross[3], cross[0], cross[1]]
        crosses.remove(cross1)
    cost += 1e12 * len(crosses)

    substation_cables_check = 0
    for i in range(0, cM.nNodes):
        if sol[cM.yij_index(i, substation)] == 1:
            substation_cables_check += 1

    if substation_cables_check > substation_cables_capacity:
        cost += 1e12 * (substation_cables_check-substation_cables_capacity)

    best_sol = sol
    best_cost = cost

    cM.draw_solution(sol=sol, intersections=True)

    timeout = time.time() + timeo
    start_time = time.time()
    while time.time() < timeout:
        tmp_delta = 10e18
        iteration += 1
        print("Iteration " + str(iteration) + ", time spent " + str(time.time()-start_time))

        for i in range(0, cM.nNodes):
            if i == substation:
                continue
            for j in range(0, cM.nNodes):
                if (succ[i] == j) or (j == i):
                    continue
                if createsLoop(succ, i, j):
                    continue
                if isTabu(i, j, taboo_list):
                    continue

                delta = cM.calculate_cost_modified(sol.copy(), i, j, succ.copy())
                for cross in crosses:
                    if (cross[0] == i and cross[1] == succ[i]) or (cross[2] == i and cross[3] == succ[i]):
                        delta -= 1e12
                if succ[i] == substation and substation_cables > substation_cables_capacity:
                    delta -= 1e12
                if j == substation and substation_cables + 1 > substation_cables_capacity:
                    delta += 1e12

                if delta < tmp_delta:
                    tmpCrosses = cM.nocross_separation_single(i, j,
                                                              xstar=(cM.update_sol(sol.copy(), i, j, succ.copy()))[0])

                    delta += 1e12 * len(tmpCrosses)
                    if delta < tmp_delta:
                        tmp_delta = delta
                        tmp_move = [i, j]
                        tmp_crosses = tmpCrosses

        print("Delta: " + str(tmp_delta))
        # If we reached a local minimum
        if tmp_delta >= -1e-8:
            if swaps == tenure:
                tenure *= 2
            if len(taboo_list) >= tenure:
                taboo_list.remove(taboo_list[0])
                swaps += 1

            taboo_list.append([tmp_move[0], succ[tmp_move[0]]])

        cost += tmp_delta
        print("Actual solution " + str(iteration) + ": " + str(cost) + str(tmp_move))
        toRemove = []
        for cross in crosses:
            if (cross[0] == tmp_move[0] and cross[1] == succ[tmp_move[0]]) or (cross[2] == tmp_move[0] and cross[3] == succ[tmp_move[0]]):
                toRemove.append(cross)
        print("Crosses before:" + str(len(crosses)))
        print("Crosses being removed:" + str(len(toRemove)))
        for cross in toRemove:
            crosses.remove(cross)
        print("Crosses after: " + str(len(crosses)))
        if tmp_move[1] == substation:
            substation_cables += 1
        if succ[tmp_move[0]] == substation:
            substation_cables -= 1
        print("Substation cables: " + str(substation_cables))
        crosses += tmp_crosses
        [sol, succ] = cM.update_sol(sol.copy(), tmp_move[0], tmp_move[1], succ.copy())

        if len(crosses) == 0:
            if cost < best_cost:
                best_cost = cost
                best_sol = sol


        c1 = 0
        for i in range(0, cM.nNodes):
            for j in range(0, cM.nNodes):
                for k in range(0, cM.nCables):
                    c1 += sol[cM.xijk_index(i, j, k)] * cM.price(k) * cM.dist(i, j)
        print("Actual correct solution: " + str(c1))

        cM.draw_solution(sol=sol, intersections=True, printCrosses=False)

    cM.draw_solution(sol=best_sol, intersections=True, printCrosses=False)
    print("FINAL SOLUTION: " + str(best_cost))

    avg_time = timeo/iteration

    return [best_sol, best_cost, avg_time]

def createsLoop(succ, i, j):
    """ Auxiliary function, checks wether an 1-opt move into i,j would create a loop

    :param succ: tree representation
    :param i: starting node
    :param j: ending node
    :return: True if creates a loop, False otherwise
    """
    while succ[j] != j:
        if succ[j] == i:
            return True
        j = succ[j]

    return False


def isTabu(i, j, taboo_list):
    """ Checks wether a 1-opt move i,j is taboo or not

    :param i: start node
    :param j: end node
    :param taboo_list: list of taboo moves
    :return: True if it's taboo, False otherwise
    """
    for el in taboo_list:
        if el[0] == i and el[1] == j:
            return True
    return False


def getConnected(cM, succ):
    """ Works only if the substation is the first element of the list
    :param cM:
    :param succ:
    :return: The connected components of the tree (excluding the substation)
    """
    pred = [[] for _ in range(cM.nNodes)]
    for i in range(0, cM.nNodes):
        pred[succ[i]].append(i)

    substation = 0
    connected_comp = []
    for index in pred[substation]:
        if index == substation:
            continue
        comp = [index]
        subtree = pred[index]
        while len(subtree) > 0:
            comp += subtree
            tmp = []
            for i in subtree:
                tmp += pred[i]
            subtree = tmp
        connected_comp.append(comp)

    counter = 0
    for i in connected_comp:
        counter += len(i)

    return connected_comp


def getSubstationCables(cM, succ, substation):
    """ Returns a list of the cables connected directly to the substation

    :param cM: model
    :param succ: tree structure
    :param substation: substation node
    :return: list of nodes directly connected to the substation
    """
    pred = [[] for _ in range(cM.nNodes)]
    for i in range(0, cM.nNodes):
        pred[succ[i]].append(i)

    return len(pred[substation])-1
